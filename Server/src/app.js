const express = require("express");
const bp = require('body-parser');


const loggerMiddleware = require("./middleware/logger.middleware");
const postMiddleware = require("./middleware/content-type.middleware");
const errorMiddleware = require("./middleware/error.middleware");
const authenticateTokenMiddleware = require("./middleware/authenticate-token.middleware");

const app = express();


const usersRouter = require("./routes/users.route");
const cardsRouter = require("./routes/cards.route");
const cors = require("cors");
app.use(cors());

app.use(bp.json());
app.use(bp.urlencoded({ extended: true }));

//Custom middlewares
app.use(loggerMiddleware.loggerHandler);
app.post("/*",postMiddleware.contentHandler)

app.use("/users", usersRouter);
app.use("/cards", cardsRouter);



app.use(errorMiddleware.errorHandler);

module.exports = app;