const mysql = require('mysql2/promise');
const config = require('../config/db.config');

async function query(sql, params) {
  const connection = await mysql.createConnection(config.db);
  connection.connect((err) =>{
    if (err) throw err;
    console.log('database connected');
})
  const [results, ] = await connection.execute(sql, params);

  return results;
}

module.exports = {
  query
}