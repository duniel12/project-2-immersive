const express = require("express");
const CardService = require('../services/card.service');
const cardRouter = express.Router();

cardRouter
    .route("/")
    // Get all cards
    .get(async (req, res) => {
        const cards = await CardService.getAllCards();
        res.json(cards);
    })
    // Create new card
    .post(async (req, res, next) => {
        const cardData = req.body;
        if (!cardData) {
            return res.status(400).send({ error: "No data cards" });
        }

        try {
            const newcard = await CardService.addCard(cardData);
            res.json(newcard);
        } catch (err) {
            next(err);
        }
    });
    cardRouter
    .route("/:id")
    //Get by email
    .get(async (req, res) => {
        const card = await CardService.getCardById(req.params.id);

        res.json(card);
    })
    .delete(async (req, res) => {
        const card = await CardService.deleteCard(req.params.id);

        res.json(`card eliminated: ${req.params.id}`);
    })

module.exports = cardRouter;