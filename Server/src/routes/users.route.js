const express = require("express");
const UserService = require('../services/user.service');
const userRouter = express.Router();

userRouter
    .route("/")
    // Get all Users
    .get(async (req, res) => {
        const users = await UserService.getAllUsers();

        res.json(users);
    })
    // Create new user
    .post(async (req, res, next) => {
        const userData = req.body;
        if (!userData) {
            return res.status(400).send({ error: "No data users" });
        }

        try {
            const newUser = await UserService.addUser(userData);
            res.json(newUser);
        } catch (err) {
            next(err);
        }
    });
    userRouter
    .route("/:email")
    //Get by email
    .get(async (req, res) => {
        const user = await UserService.getUserByEmail(req.params.email);

        res.json(user);
    })
    .delete(async (req, res) => {
        const user = await UserService.deleteUser(req.params.email);

        res.json(`User eliminated: ${req.params.email}`);
    })

module.exports = userRouter;