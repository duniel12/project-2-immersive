const queryHelper = require('../helpers/query.helper');

class CardService {

  static async getAllCards() {
    const result = await queryHelper.query(
      `SELECT * FROM immersivelastlab.card`
    );
    return result;
  }

  static async getCardById(id) {
    const result = await queryHelper.query(
      `SELECT * FROM immersivelastlab.card WHERE idCard = ${id}`
    );
    return result;
  }

  static async getCardByNumber(number) {
    const result = await queryHelper.query(
      `SELECT * FROM immersivelastlab.card WHERE cardNumber = "${number}"`
    );
    return result;
  }

  static async addCard(CardData) {
    const insert = await queryHelper.query(
      `INSERT INTO immersivelastlab.card(cardType, cardNumber, expiration, cvs, cardUser)
      values
      ("${CardData.cardType}", "${CardData.cardNumber}","${CardData.expiration}","${CardData.cvs}", "${CardData.cardUser}" )`
    );
    const insertedCard = await this.getCardByNumber(CardData.cardNumber);

    return insertedCard;
  }

  static async updateCard(id, newData) {
    const update = await queryHelper.query(
      `UPDATE immersivelastlab.card 
      SET cardType="${newData.cardType}", cardNumber="${newData.cardNumber}", expiration="${newData.expiration}", cvs="${newData.cvs}", cardUser="${newData.cardUser}" 
      WHERE idCard=${id} `
    )
  const updatedCard = await this.getCardById(newData.idCard);

    return updatedCard;
  }

  static async deleteCard(id) {
    await queryHelper.query(
      `DELETE FROM immersivelastlab.card 
   WHERE idCard="${id}" `
    )


    return "Card deleted"; 
  }
}

module.exports = CardService;

