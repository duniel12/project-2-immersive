const mockingoose = require('mockingoose');
const Book = require("../models/book.model");
const BookService = require("../services/book.service");


const defBooks = [{
    "_id": "62b0dd69f46e1df0f61fa583",
    "title": "shorts",
    "book": "62b0dcadf46e1df0f61fa56f",
    "published_at": "2000-10-10T06:00:00.000Z",
    "__v": 0
},
{
    "_id": "62b0dd7ef46e1df0f61fa585",
    "title": "Harry Potter",
    "book": "62b0dcadf46e1df0f61fa56f",
    "published_at": "2000-05-18T06:00:00.000Z",
    "__v": 0
},
{
    "_id": "62b0dda6f46e1df0f61fa588",
    "title": "React",
    "book": "62b0d9ffd4f237cd4661ef16",
    "published_at": "2018-03-21T06:00:00.000Z",
    "__v": 0
}];

describe("Books service tests", () => {
    test("get all books", async () => {
        mockingoose(Book).toReturn(defBooks, "find");
        const books = await BookService.getAllBooks();
        expect(books.map((book) => book.title)).toEqual(["shorts","Harry Potter","React"])
    });
    test("get book by id (compare title)", async () => {
        mockingoose(Book).toReturn(defBooks[2], "findOne");
        const book = await BookService.getBookById({_id:"62b0dda6f46e1df0f61fa588"});
        expect(book.title).toEqual("React");
    });

    test("put book title", async () => {
        mockingoose(Book).toReturn(defBooks[0], "findOneAndUpdate");
        const book = await BookService.updateBook({_id: "62b0dda6f46e1df0f61fa588"},{title: "shorts"});
        expect(book.title).toEqual("shorts");
    });
    test("delete book", async () => {
        mockingoose(Book).toReturn(defBooks[0], "findOneAndDelete");
        const book = await BookService.deleteBook({_id: "62b0dda6f46e1df0f61fa588"});
        expect(book.title).toEqual("shorts");
    });
});