const queryHelper = require('../helpers/query.helper');

class UserService {

  static async getAllUsers() {
    const result = await queryHelper.query(
      `SELECT * FROM immersivelastlab.user`
    );
    return result;
  }

  static async getUserById(id) {
    const result = await queryHelper.query(
      `SELECT * FROM immersivelastlab.user WHERE idUser = ${id}`
    );
    return result;
  }

  static async getUserByEmail(email) {
    const result = await queryHelper.query(
      `SELECT * FROM immersivelastlab.user WHERE email = "${email}"`
    );
    return result;
  }

  static async addUser(userData) {
    const insert = await queryHelper.query(
      `INSERT INTO immersivelastlab.user(username, email, userPassword, nameUser, lastName, birthDate, gender)
      values 
      ("${userData.username}", "${userData.email}","${userData.userPassword}","${userData.nameUser}","${userData.lastName}","${userData.birthDate}","${userData.gender}" )`
    );
    const insertedUser = await this.getUserByEmail(userData.email);

    return insertedUser;
  }

  static async updateUser(email, newData) {
    const update = await queryHelper.query(
      `UPDATE immersivelastlab.user 
      SET username="${newData.username}", email="${newData.email}", userPassword="${newData.userPassword}", nameUser="${newData.nameUser}", lastName="${newData.lastName}", birthDate="${newData.birthDate}", gender="${newData.gender}"
      WHERE email="${email}" `
    )
  const updatedUser = await this.getUserByEmail(newData.email);

    return updatedUser;
  }

  static async deleteUser(email) {
    await queryHelper.query(
      `DELETE FROM immersivelastlab.user 
   WHERE email="${email}" `
    )


    return "User deleted"; 
  }
}

module.exports = UserService;

