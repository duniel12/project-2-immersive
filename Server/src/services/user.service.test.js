const mockingoose = require('mockingoose');
const User = require("../models/user.model");
const UserService = require("../services/user.service");


const defUsers = [{
    "_id": "62b201435c21bc12e0a35b17",
    "fullName": "Daniel",
    "username": "d@gmail.com",
    "password":
        "$2b$10$gSzq4KcNFktleOICK7qfQOKtsSQoeQJbcHaENgKzVQ86r/aM6OaUi",
    "__v": 0
},
{
    "_id": "62b2404accfab222b0e43899",
    "fullName": "Peri",
    "username": "peri@gmail.com",
    "password": "$2b$10$npntm0wM3MVz5TkEq2mZcew7RQNhunDjhfAS5Se0RtC1Dm.Ty2Tki",
    "__v": 0
}];

describe("users service tests", () => {
    test("get all users", async () => {
        mockingoose(User).toReturn(defUsers, "find");
        const users = await UserService.getAllUsers();
        expect(users.map((user) => user.username)).toEqual(["d@gmail.com", "peri@gmail.com"])
    });
    test("get user by id (compare username)", async () => {
        mockingoose(User).toReturn(defUsers[0], "findOne");
        const user = await UserService.getUserById({ _id: "62b201435c21bc12e0a35b17" });
        expect(user.username).toEqual("d@gmail.com");
    });
    test("get user by username ", async () => {
        mockingoose(User).toReturn(defUsers[0], "findOne");
        const user = await UserService.getUserByUsername("d@gmail.com");
        expect(user.username).toEqual("d@gmail.com");
    });

    test("put user username", async () => {
        mockingoose(User).toReturn(defUsers[0], "findOneAndUpdate");
        const user = await UserService.updateUser({ _id: "62b201435c21bc12e0a35b17" }, { username: "daniel@gmail.com" });
        expect(user.username).toEqual("daniel@gmail.com");
    });
    test("delete user", async () => {
        mockingoose(User).toReturn(defUsers[0], "findOneAndDelete");
        const user = await UserService.deleteUser({ _id: "62b201435c21bc12e0a35b17" });
        expect(user.username).toEqual("d@gmail.com");
    });
});