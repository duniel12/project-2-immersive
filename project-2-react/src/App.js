import React, {useContext} from 'react';
import { Outlet, useLocation } from 'react-router-dom';


import {ApiContext} from './components/Context/ApiContextProvider';

import Loader from './components/Loader/Loader';
import Home from './components/Home/Home';


const App = () => {
    const path = useLocation();
    const { isLoading } = useContext(ApiContext);
    //console.log(products, isLoading);

    if (isLoading) {
        return (
            <Loader type={"loader"}/>
        );
    } else {
        return (
            <div>
                {path.pathname === "/" && <Home />}
                <Outlet />
                
            </div>
        );
    }
}

export default App;