import { Link } from 'react-router-dom';
import PropTypes from "prop-types";

const Card = (props) => {
    const { productId, imgSrc, imgSrcHover, imgAlt, cardTitle, cardPrice, cardCntClass, cardThemeClass } = props;
    if(cardThemeClass === "dark") {
        return (
            <Link className={cardCntClass} to={`/product/${productId}`}>
                <img src={imgSrc}
                    alt={imgAlt} className="card__img" />
                <img src={imgSrcHover}
                    alt={imgAlt} className="card__img card__img--hover" />
                <div className="card__price">
                    <span className="card__price--text">
                        ${cardPrice}
                    </span>
                </div>
    
                <div className="card__info card__info--dark" >
                    <span className={"card__info--spacing card__info--size card__title card__title--dark"}>
                        {cardTitle}
                    </span>
                    
                </div>
            </Link>
        );
    } else {
        return (
            <Link className={cardCntClass} to={`/product/${productId}`}>
                <img src={imgSrc}
                    alt={imgAlt} className="card__img" />
                <img src={imgSrcHover}
                    alt={imgAlt} className="card__img card__img--hover" />
                <div className="card__price">
                    <span className="card__price--text">
                        ${cardPrice}
                    </span>
                </div>
    
                <div className="card__info" >
                    <span className={`card__info--spacing card__info--size card__title ${cardThemeClass}`}>
                        {cardTitle}
                    </span>
                    
                </div>
            </Link>
        );
    }
    
}

Card.propTypes = {
    productId: PropTypes.string,
    imgSrc: PropTypes.string,
    imgSrcHover: PropTypes.string,
    imgAlt: PropTypes.string,
    cardTitle: PropTypes.string,
    cardPrice: PropTypes.string,
    cardCntClass: PropTypes.string,
    cardThemeClass: PropTypes.string,
}

export default Card;