import Card from "../Card/Card";
import PropTypes from "prop-types";

const CardsGenerator = (props) => {
    const { products, cardThemeClass } = props;
    return (
        <div className="products__cnt">
            {products.map((product) => {
                if (product.name !== "") {
                    let firstKeyImg = Object.keys(product.images[0])[0];
                    let productImgs = product.images[0];

                    let productFirstImg = productImgs[firstKeyImg][0];
                    let productHoverImg = productImgs[firstKeyImg][1];

                    return (<Card key={product.id} productId={product.id} imgSrc={productFirstImg} imgSrcHover={productHoverImg} imgAlt=
                        {product.name} cardTitle={product.name} cardPrice={product.price} cardCntClass={"products__cnt__card"} cardThemeClass={cardThemeClass}/>
                    );
                }


            })}
        </div>
    );
}
CardsGenerator.propTypes = {
    products: PropTypes.array.isRequired,
    cardThemeClass: PropTypes.string,
}
export default CardsGenerator;