import { useState } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import PropTypes from 'prop-types';

const CarouselLoader = (props) => {
    const [index, setIndex] = useState(0);
    const {images} = props;

    const handleSelect = (selectedIndex, e) => {
      setIndex(selectedIndex);
    };

    return (
      <Carousel activeIndex={index} onSelect={handleSelect}>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={images[0]}
            alt="First slide"
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={images[1]}
            alt="Second slide"
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={images[2]}
            alt="Third slide"
          />
        </Carousel.Item>
      </Carousel>
    );
  }
  
CarouselLoader.propTypes = {
  images: PropTypes.array,
}

export default CarouselLoader;