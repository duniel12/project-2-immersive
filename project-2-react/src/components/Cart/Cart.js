import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import SearchResult from '../SearchResult/SearchResult';
import Navbar from '../Navbar/Navbar';
import Footer from '../Footer/Footer';
import ItemQuantity from '../ItemQuantity/ItemQuantity';


const Cart = (props) => {
    const { displayOnly, forCheckout } = props;
    let localCart = JSON.parse(localStorage.getItem('cart'));
    let cartAmount = 0;

    if (localCart !== null) {
        if (localCart.length > 0) {
            for (let i = 0; i < localCart.length; i++) {
                cartAmount += localCart[i].product.price * localCart[i].quantity;
            }
        }
    }

    const [cartUpdated, setCartUpdated] = useState(false);
    const [search, setSearch] = useState(false);
    const [total, setTotal] = useState(cartAmount);
    const [localProducts, setLocalProducts] = useState(localCart);

    //Toggle search section
    const onClickSearch = () => {
        if (search === false) {
            setSearch(true);
        } else {
            setSearch(false);
        }
    }


    const removeItem = (item, remove) => {
        if (remove) {
            let newLocalProducts = localProducts.filter((product) => !(product.color === item.item.color && product.size === item.item.size && product.product.name === item.item.product.name));
            localStorage.setItem('cart', JSON.stringify(newLocalProducts));
            setCartUpdated(true);
        }
    }
    const updateQuantity = (item, quantity) => {
        let newLocalProducts = localProducts;
        for (let i = 0; i < newLocalProducts.length; i++) {
            if (newLocalProducts[i].color === item.item.color && newLocalProducts[i].size === item.item.size && newLocalProducts[i].product.name === item.item.product.name) {
                if (quantity === "more") {
                    if (item.item.quantity >= 1) {
                        newLocalProducts[i].quantity += 1;
                    }

                } else if (quantity === "less") {
                    if (item.item.quantity > 1) {
                        newLocalProducts[i].quantity -= 1;
                    }

                }
            }
        }
        localStorage.setItem('cart', JSON.stringify(newLocalProducts));
        setCartUpdated(true);

    }
    useEffect(() => {
        if (cartUpdated) {
            setLocalProducts(JSON.parse(localStorage.getItem('cart')));
            setTotal(cartAmount);
            setCartUpdated(false);
        }
    }, [cartUpdated, cartAmount]);

    if (localProducts !== null) {
        if (localProducts.length > 0) {
            return (
                <div>
                    {!displayOnly && <Navbar onClickSearch={onClickSearch} />}
                    {search && <SearchResult onClickSearch={onClickSearch} />}
                    {!search && (localProducts !== null || localProducts.length > 0) && <div className="cart__root">
                        {!displayOnly && <h1 className="cart__info--text cart__title">Shopping cart</h1>}

                        <table className={`cart__table-products__cnt ${forCheckout}`}>

                            <thead className="cart__table-products__header">
                                <tr>
                                    <th colSpan={2} className="cart__table-products__header--items cart__table-products__header--items cart__table-products__header--title">
                                        Items
                                    </th>
                                    <th className="cart-show cart__table-products__header--amount cart__table-products__header--items cart__table-products__header--quantity">
                                        Quantity
                                    </th>
                                    <th className="cart-show cart__table-products__header--items cart__table-products__header--amount cart__table-products__header--subtotal">
                                        Subtotal
                                    </th>
                                </tr>
                            </thead>
                            <tbody className="cart__table-products__body">
                                {localProducts.map((item) => {
                                    return (
                                        <tr key={item.product.id + item.size + item.color} className="cart__table-products__row">
                                            <td className="cart__table-products__cnt-img">
                                                <img src={item.image} className="cart__table-products__cell--spacing cart__table-products__cell-img" alt={item.product.name} />
                                            </td>
                                            <td className="cart__table-products__cell-info">

                                                <div>
                                                    <p className="cart__table-products__info--text">{item.product.name}</p>
                                                    <p className="cart__table-products__info--text">Color: {item.color}</p>
                                                    <p className="cart__table-products__info--text">Price: ${item.product.price}</p>
                                                    <p className="cart__table-products__info--text">Size: {item.size}</p>
                                                </div>

                                                {!displayOnly && <div className="cart__table-products__cell-quantity--1">
                                                    <ItemQuantity quantity={item.quantity} item={{ item }} updateQuantity={updateQuantity} removeItem={removeItem} />
                                                </div>}


                                            </td>



                                            <td className="cart__table-products__cell-subtotal">
                                                <p>
                                                    Subtotal: ${item.quantity * item.product.price}
                                                </p>
                                            </td>
                                        </tr>
                                    );
                                })
                                }
                            </tbody>
                            <tfoot className="cart__footer">
                                <tr>
                                    <td >
                                        <p className="cart__footer__title">
                                            Total: ${total}
                                        </p>
                                    </td>
                                </tr>
                            </tfoot>

                        </table>
                        {!displayOnly && <div className="product__add__root cart__checkout__cnt">
                            <Link to={"/checkout"} className="cart__checkout__bt-cnt">
                                <button className="product__add__btn cart__checkout__btn">
                                    Checkout
                                </button>
                            </Link>

                        </div>}
                    </div>
                    }

                    {!displayOnly && <Footer />}
                </div>
            );
        } else {
            return (
                <div>
                    <Navbar onClickSearch={onClickSearch} />
                    {search && <SearchResult onClickSearch={onClickSearch} />}
                    <h1 className="cart__info--text cart__title">No products in cart</h1>

                </div>
            );
        }
    } else {
        return (
            <div>
                <Navbar onClickSearch={onClickSearch} />
                {search && <SearchResult onClickSearch={onClickSearch} />}
                <h1 className="cart__info--text cart__title">No products in cart</h1>

            </div>
        );
    }
}

Cart.propTypes = {
    displayOnly: PropTypes.bool,
    forCheckout: PropTypes.string,
}

export default Cart;