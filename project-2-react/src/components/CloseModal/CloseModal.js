import PropTypes from "prop-types";


const CloseModal = (props) => {
    const {onClickSearch} = props;    
    return (
        <button className="filter__remove--close" onClick={onClickSearch}>
            <i className="filter__remove--icon fa fa-close"></i>
        </button>
    );
}

CloseModal.propTypes = {
    onClickSearch: PropTypes.func,
}

export default CloseModal;
