import React, { useEffect, useState, createContext, useContext } from 'react';
import App from '../../App';

export const ApiContext = createContext({products: null, isLoading: null});

const ApiContextProvider = () => {
    const [products, setProducts] = useState({});
    const [apiError, setApiError] = useState(null);
    const [loading, setLoading] = useState(true);
    useEffect(() => {
        fetch('http://localhost:3030/products')
            .then(response => {
                if (response.ok) {
                    return response.json()
                }
                throw response;
            })
            .then(data => {
                console.log(data);
                setProducts(data.products);
            })
            .catch(error => {
                console.error("Error fetching data: ", error);
                setApiError(error);
            })
            .finally(() => {
                let timer1 = setTimeout(() => setLoading(false), 1.5 * 1000);
                return () => {
                    clearTimeout(timer1);
                };

            })
    }, []);

    return (
        <ApiContext.Provider value={{ products: products, isLoading: loading }}>
            <App />
        </ApiContext.Provider>
    );

};

export default ApiContextProvider;

export function useApiContext() {
    return useContext(ApiContext);
}

