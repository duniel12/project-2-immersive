import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

import Navbar from "../Navbar/Navbar";
import Cart from "../Cart/Cart";
import Loader from "../Loader/Loader";

const Checkout = () => {
    const [name, setName] = useState("");
    const [lastName, setLastName] = useState("");
    const [address, setAddress] = useState("");
    const [city, setCity] = useState("");
    const [country, setCountry] = useState("");
    const [phone, setPhone] = useState("");
    const [cardNumber, setCardNumber] = useState("");
    const [cardExp, setCardExp] = useState("");
    const [cardPin, setCardPin] = useState("");
    const [error, setError] = useState("");
    const [completed, setCompleted] = useState(false);

    let formInfo = [{
        info: "Name",
        id: "form-personal-name",
        type: "text",
        placeholder: "Name",
        errorInfo: "Enter a name", 
        customClassInput: "",
        labelRequired: true
    },{
        info: "Last name",
        id: "form-personal-last-name",
        type: "text",
        placeholder: "Last name",
        errorInfo: "Enter a last name", 
        customClassInput: "",
        labelRequired: true
    },{
        info: "Date of birth",
        id: "",
        type: "date",
        placeholder: "",
        errorInfo: "", 
        customClassInput: "",
        labelRequired: true
    },{
        info: "",
        id: "",
        type: "radio",
        placeholder: "",
        errorInfo: "", 
        customClassInput: "",
        labelRequired: true
    }]
    

    let handleInputChange = (e, inp) => {
        if (inp === "name") {
            setName(e.target.value);
        } else if (inp === "lastName") {
            setLastName(e.target.value);
        } else if (inp === "address") {
            setAddress(e.target.value);
        } else if (inp === "city") {
            setCity(e.target.value);
        } else if (inp === "country") {
            setCountry(e.target.value);
        } else if (inp === "phone") {
            setPhone(e.target.value);
        } else if (inp === "cardNumber") {
            setCardNumber(e.target.value);
        } else if (inp === "cardExp") {
            setCardExp(e.target.value);
        } else if (inp === "cardPin") {
            setCardPin(e.target.value);
        }
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        let formValid = [true,true,true,true,true,true,true,true,true];
        //Name validation
        if (name.length === 0) {
            formValid[0] = false;
            document.getElementById("checkout-shipping-name-error").classList.add("checkout__error--label");
            document.getElementById("checkout-shipping-name").classList.add("checkout__error--inp");
        } else {
            formValid[0] = true;
            document.getElementById("checkout-shipping-name-error").classList.remove("checkout__error--label");
            document.getElementById("checkout-shipping-name").classList.remove("checkout__error--inp");
        }
        //Last name validation
        if (lastName.length === 0) {
            formValid[1] = false;
            document.getElementById("checkout-shipping-last_name-error").classList.add("checkout__error--label");
            document.getElementById("checkout-shipping-last_name").classList.add("checkout__error--inp");
        } else {
            formValid[1] = true;
            document.getElementById("checkout-shipping-last_name-error").classList.remove("checkout__error--label");
            document.getElementById("checkout-shipping-last_name").classList.remove("checkout__error--inp");
        }
        //Address validation
        if (address.length === 0) {
            formValid[2] = false;
            document.getElementById("checkout-shipping-address-error").classList.add("checkout__error--label");
            document.getElementById("checkout-shipping-address").classList.add("checkout__error--inp");
        } else {
            formValid[2] = true;
            document.getElementById("checkout-shipping-address-error").classList.remove("checkout__error--label");
            document.getElementById("checkout-shipping-address").classList.remove("checkout__error--inp");
        }
        //City validation
        console.log(city)
        if (city.length === 0) {
            formValid[3] = false;
            document.getElementById("checkout-shipping-city-error").classList.add("checkout__error--label");
            document.getElementById("checkout-shipping-city").classList.add("checkout__error--inp");
        } else {
            formValid[3] = true;
            document.getElementById("checkout-shipping-city-error").classList.remove("checkout__error--label");
            document.getElementById("checkout-shipping-city").classList.remove("checkout__error--inp");
        }
        //Country validation
        if (country.length === 0) {
            formValid[4] = false;
            document.getElementById("checkout-shipping-country-error").classList.add("checkout__error--label");
            document.getElementById("checkout-shipping-country").classList.add("checkout__error--inp");
        } else {
            formValid[4] = true;
            document.getElementById("checkout-shipping-country-error").classList.remove("checkout__error--label");
            document.getElementById("checkout-shipping-country").classList.remove("checkout__error--inp");
        }
        //Phone validation
        if (phone.length === 0 ) {
            formValid[5] = false;
            document.getElementById("checkout-shipping-phone-error").classList.add("checkout__error--label");
            document.getElementById("checkout-shipping-phone").classList.add("checkout__error--inp");
        } else if(/^(?=(.*\d){8,})[\d\(\)\s+-]{8,}$/.test(phone)) {
            formValid[5] = true;
            document.getElementById("checkout-shipping-phone-error").classList.remove("checkout__error--label");
            document.getElementById("checkout-shipping-phone").classList.remove("checkout__error--inp");
        } else {
            formValid[5] = false;
            document.getElementById("checkout-shipping-phone-error").classList.add("checkout__error--label");
            document.getElementById("checkout-shipping-phone").classList.add("checkout__error--inp");
        }
        //Card number validation
        if (cardNumber.length === 0 ) {
            formValid[6] = false;
            document.getElementById("checkout-payment-card_number-error").classList.add("checkout__error--label");
            document.getElementById("checkout-payment-card_number").classList.add("checkout__error--inp");
        } else if(/^\d+$/.test(cardNumber)) {
            formValid[6] = true;
            document.getElementById("checkout-payment-card_number-error").classList.remove("checkout__error--label");
            document.getElementById("checkout-payment-card_number").classList.remove("checkout__error--inp");
        } else {
            formValid[6] = false;
            document.getElementById("checkout-payment-card_number-error").classList.add("checkout__error--label");
            document.getElementById("checkout-payment-card_number").classList.add("checkout__error--inp");
        }
        //Card expiration validation
        console.log(cardExp);
        if (cardExp.length === 0) {
            formValid[7] = false;
            document.getElementById("checkout-payment-card_exp-error").classList.add("checkout__error--label");
            document.getElementById("checkout-payment-card_exp").classList.add("checkout__error--inp");
        } else {
            formValid[7] = true;
            document.getElementById("checkout-payment-card_exp-error").classList.remove("checkout__error--label");
            document.getElementById("checkout-payment-card_exp").classList.remove("checkout__error--inp");
        }
        //Card pin validation
        if (cardPin.length === 0 ) {
            formValid[8] = false;
            document.getElementById("checkout-payment-cv-error").classList.add("checkout__error--label");
            document.getElementById("checkout-payment-cv").classList.add("checkout__error--inp");
        } else if(/^\d+$/.test(cardPin)) {
            formValid[8] = true;
            document.getElementById("checkout-payment-cv-error").classList.remove("checkout__error--label");
            document.getElementById("checkout-payment-cv").classList.remove("checkout__error--inp");
        } else {
            formValid[8] = false;
            document.getElementById("checkout-payment-cv-error").classList.add("checkout__error--label");
            document.getElementById("checkout-payment-cv").classList.add("checkout__error--inp");
        }
        

        //When everythings validated
        let formValidFinal = true;
        for(let i=0; i < formValid.length; i++) {
            if(formValid[i] === false) {
                formValidFinal = false;
            }
        }
        if (formValidFinal === true) {
            setError("");
            setCompleted(true);
            
        } else {
            setError("checkout__error--show");
        }

    }

    useEffect(() => {
        if(completed) {

        }
    }, [completed]);

    return (
        <div>
            {completed && <Loader type={"info"} withLoader={true} />}
            <Navbar justLogo={true} />
            <div className="checkout__root">
                <div className="checkout__cnt checkout__products__cnt">
                    <Cart displayOnly={true} forCheckout={"checkout__cart__table"} /> 
                </div>
                <div className="checkout__cnt checkout__info__cnt">
                    <h1 className="checkout__info__title">Checkout</h1>
                    <div className={`checkout__error ${error}`}>
                        <i className='fas fa-exclamation-circle checkout__error--icon'></i><p className="checkout__error--text">Wrong or missing information. Check shipping and payment information again.</p>
                    </div>
                    
                </div>
            </div>
        </div>
    );
}

export default Checkout;