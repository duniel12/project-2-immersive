const Footer = () => {
    return(
        <footer className="footer__root">
        <div className="footer__info">
            <p className="footer__copyright"><i className="fa fa-solid fa-copyright"></i> GymRon 2022.</p>
        </div>
        <div className="footer__links__root">

            <div className="footer__links__social">
                <a href="/" className="footer__links" aria-label="facebook footer link" title="Facebook"><i
                        className="fa fa-brands fa-facebook footer__links__icons"></i></a>
                <a href="/" className="footer__links" aria-label="twitter footer link " title="Twitter"><i
                        className="fa fa-brands fa-twitter footer__links__icons"></i></a>
                <a href="/" className="footer__links" aria-label="instagram footer link" title="Instagram"><i
                        className="fa fa-brands fa-instagram footer__links__icons"></i></a>
            </div>

        </div>

    </footer>
    );
}

export default Footer;