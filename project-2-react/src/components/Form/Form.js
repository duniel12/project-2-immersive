import { useState, useRef } from "react";
import { PropTypes } from "prop-types";

const Form = (props) => {
    const {returnFormInfo, inputs} = props;
    

    let inputInfo = {
        info: "",
        id: "",
        placeholder: "",
        errorInfo: "", 
        customClassInput: ""
    };
    return (
        <form className="checkout__form" onSubmit={handleSubmit}>
            <div className="checkout__form__shipping">
                <h2 className="checkout__form__title">Shipping information</h2>


                <label for="checkout-shipping-last_name" hidden >Last name</label>
                <input type="text" aria-labelledby="checkout-shipping-last_name" placeholder="Last name" id="checkout-shipping-last_name" className="checkout__form__inp" onChange={e => handleInputChange(e, "lastName")} />
                <p className="checkout__error" id="checkout-shipping-last_name-error">Enter a last name</p>

                <label type="text" for="checkout-shipping-address" hidden >Address</label>
                <input type="text" aria-labelledby="checkout-shipping-address" placeholder="Address" id="checkout-shipping-address" className="checkout__form__inp" onChange={e => handleInputChange(e, "address")} />
                <p className="checkout__error" id="checkout-shipping-address-error">Enter an address</p>

                <label type="text" for="checkout-shipping-city" hidden >City</label>
                <input type="text" aria-labelledby="checkout-shipping-city" placeholder="City" id="checkout-shipping-city" className="checkout__form__inp" onChange={e => handleInputChange(e, "city")} />
                <p className="checkout__error" id="checkout-shipping-city-error">Enter a city</p>

                <label for="checkout-shipping-country" hidden >Country</label>
                <input type="text" aria-labelledby="checkout-shipping-country" placeholder="Country" id="checkout-shipping-country" className="checkout__form__inp" onChange={e => handleInputChange(e, "country")} />
                <p className="checkout__error" id="checkout-shipping-country-error">Enter a country</p>

                <label for="checkout-shipping-phone" hidden >Phone</label>
                <input type="text" aria-labelledby="checkout-shipping-phone" placeholder="Phone" id="checkout-shipping-phone" className="checkout__form__inp" onChange={e => handleInputChange(e, "phone")} />
                <p className="checkout__error" id="checkout-shipping-phone-error">Must be a valid phone with numbers</p>

            </div>
            <div className="checkout__form__payment">
                <h2 className="checkout__form__title">Payment</h2>
                <label for="checkout-payment-card_number" hidden>Card number</label>
                <input type="text" aria-labelledby="checkout-payment-card_number" placeholder="Card number" id="checkout-payment-card_number" className="checkout__form__inp" onChange={e => handleInputChange(e, "cardNumber")} />
                <p className="checkout__error" id="checkout-payment-card_number-error">Enter a valid card number</p>

                <label for="checkout-payment-card_exp" hidden>Expiration date</label>
                <input type="month" min={"2022-06"} aria-labelledby="checkout-payment-card_exp" placeholder="Country" id="checkout-payment-card_exp" className="checkout__form__inp" onChange={e => handleInputChange(e, "cardExp")} />
                <p className="checkout__error" id="checkout-payment-card_exp-error">Enter a valid expiration date</p>

                <label for="checkout-payment-cv" hidden>Security code (CV) </label>
                <input type="text" aria-labelledby="checkout-payment-cv" placeholder="Security code" id="checkout-payment-cv" className="checkout__form__inp" onChange={e => handleInputChange(e, "cardPin")} />
                <p className="checkout__error" id="checkout-payment-cv-error">Enter a valid security code</p>
            </div>
            <button name="submit-btn" type="submit" className="checkout__form__btn product__add__btn">
                Purchase
            </button>
        </form>
    );
}

Form.propTypes = {
    returnFormInfo: PropTypes.func
}

export default Form;