import { useState, useRef } from "react";
import { PropTypes } from "prop-types";
 

const FormInput = (props) => {
    const {labelRequired, inputInfo, handleInputChange} = props;

    return (
        <div className="form__root">
            {labelRequired && <label for={id} hidden >{info}</label>}

            <input type={`${inputInfo.type}`} aria-labelledby={id} placeholder={placeholder} id={id} className={`checkout__form__inp ${customClassInput}`} onChange={e => handleInputChange(e, info)} />

            <p className="checkout__error" id={`${id}-error`}>{errorInfo}</p>
        </div>
    );
}


FormInput.propTypes = {
    labelRequired: PropTypes.bool,
    inputInfo: PropTypes.obj,
    handleInputChange: PropTypes.func,
}
export default FormInput;