import React, { useContext, useState } from 'react';


import Navbar from '../Navbar/Navbar';
import Footer from '../Footer/Footer';
import Card from '../Card/Card';
import { ApiContext } from '../Context/ApiContextProvider';
import SearchResult from '../SearchResult/SearchResult';

const Home = () => {
    const { products } = useContext(ApiContext);
    const [search, setSearch] = useState(false);
    let randomProducts = randomNumArray(13);

    //Functions just for randomizing the cards section on the landing
    function randomNumArray(max) {
        const numbers = Array(max).fill().map((_, index) => index + 1);
        numbers.sort(() => Math.random() - 0.5);

        return numbers.slice(0, 6);
    }
    const RandomCard = (props) => {
        let indexRandomProduct = randomProducts[props.indexOfCard];
        let randomProduct = products[indexRandomProduct];

        let productImgs = randomProduct.images[0];
        let firstKeyImg = Object.keys(productImgs)[0];
        let productFirstImg = productImgs[firstKeyImg][0];
        let productHoverImg = productImgs[firstKeyImg][1];
        return (
            <Card productId={randomProduct.id} imgSrc={productFirstImg} imgSrcHover={productHoverImg} imgAlt=
                {randomProduct.name} cardTitle={randomProduct.name} cardPrice={randomProduct.price} cardCntClass={"cards__card"} />
        );
    }

    //Toggle search section
    const onClickSearch = () => {
        if (search === false) {
            setSearch(true);
        } else {
            setSearch(false);
        }
    }



    return (
        <div>
            <Navbar onClickSearch={onClickSearch} />
            {search && <SearchResult onClickSearch={onClickSearch} />}
            {!search &&
                <div>
                    <main className="main__root">
                        <section className="hero__root">
                            <div className="hero__block hero__block__1">
                                <h2 className="hero__block__element hero__block__element--title">For the people</h2>
                                <button className="general__btn hero__block__element hero__block__buton">Shop now</button>
                            </div>

                        </section>
                        <section className="accesories__root">
                            <div className="accesories__block accesories__block__acc">
                                <img src="https://cdn.shopify.com/s/files/1/0156/6146/products/SharkheadCapBlackI1A6R-BBBB001-Edit_BK_885x.jpg?v=1647865065"
                                    alt="Black cap GymRon" className="accesories__block__img" />
                            </div>
                            <div className="accesories__block accesories__block__info accesories__block__info--1">
                                <h2 className="accesories__block__title">Accesories</h2>
                                <div className="accesories__block__buttons">
                                    <button className="general__btn">
                                        Shop accessories
                                    </button>
                                    <button className="general__btn">
                                        Shop equipment
                                    </button>
                                </div>
                            </div>
                            <div className="accesories__block accesories__block__info accesories__block__info--2">
                                <h2>More products</h2>
                                <div className="accesories__block__buttons">
                                    <button className="general__btn">
                                        Shop more
                                    </button>
                                </div>
                            </div>
                            <div className="accesories__block accesories__block__eq">
                                <img src="https://cdn.shopify.com/s/files/1/0156/6146/products/1.3LWaterBottlePaigePinkI1A2J-KBCH4_ZH_ZH_9f66fda4-c2f6-4c69-9ae0-9f667021e433_438x.jpg?v=1650999027"
                                    alt="Orange bottle GymRon" className="accesories__block__img" />
                            </div>

                        </section>
                        <section className="cards__root">
                            <div className="cards__title__cnt">
                                <h1 className="cards__title">Main products</h1>
                            </div>

                            <div className="cards__block cards__block__1">

                                <div className="cards__row cards__row--left">
                                    {<RandomCard indexOfCard={0} />
                                    }

                                    {<RandomCard indexOfCard={1} />
                                    }

                                </div>


                                <div className="cards__row">
                                    {<RandomCard indexOfCard={2} />
                                    }

                                    <div className="cards__card--special"></div>

                                    {<RandomCard indexOfCard={3} />
                                    }
                                </div>

                                <div className="cards__row">
                                    {<RandomCard indexOfCard={4} />
                                    }

                                    <div className="cards__card--special"></div>

                                    {<RandomCard indexOfCard={5} />
                                    }
                                </div>
                            </div>

                        </section>

                        <section className="signme__root">
                            <div className="signme__cnt--1">
                                <div className="signme__cnt">
                                    <h2>First time discount</h2>
                                    <h3 className="signme__cnt__subtitle">Sign up now to enjoy!</h3>
                                    <p className="signme__cnt__info">Discount for the first time purchase for all of our great variety of products</p>
                                    <form action="" className="signme__form">
                                        <label htmlFor="signme-inp-email" className="signme__form__label" id="signme-label">Email</label>
                                        <input type="email" name="signme-inp-email" placeholder="daniel@gmail.com" className="signme__form__inp"
                                            id="signme-inp-email" aria-labelledby="signme-label" />
                                    </form>


                                    <button aria-label="Submit form button" className="general__btn">Get discount</button>

                                </div>
                            </div>
                        </section>

                    </main>
                    <Footer />
                </div>
            }
        </div>
    );
}

export default Home;