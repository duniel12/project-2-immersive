
import PropTypes from 'prop-types';
const ItemQuantity = (props) => {
    const { quantity, updateQuantity, removeItem, item } = props;

    return (
        <div className="item-quantity__root">
            <div className="item-quantity__cnt">
                <button className="item-quantity__btn item-quantity__btn--clickable" onClick={() =>
                    updateQuantity(item,"less")
                }>
                    -
                </button>
                <button className="item-quantity__btn" disabled>
                    {quantity}
                </button>
                <button className="item-quantity__btn item-quantity__btn--clickable" onClick={
                    () => updateQuantity(item,"more")
                }>
                    +
                </button>
            </div>
            <button className=" item-quantity__remove" onClick={() => removeItem(item, true)}>Remove</button>
        </div>
    );
}

ItemQuantity.propTypes = {
    quantity: PropTypes.number,
    updateQuantity: PropTypes.func,
    removeItem: PropTypes.func,
}

export default ItemQuantity;