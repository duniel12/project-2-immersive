import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';

const Loader = (props) => {
    const { type } = props;
    const [withLoader, setWithLoader] = useState(true);
    const [info, setInfo] = useState("Validating purchase");
    const navigate = useNavigate();
    const [completed, setCompleted] = useState(false);

    let timer1 = setTimeout(() => setWithLoader(false), 3 * 1000);

    const goHome = () => {
        return navigate("/");
    }

    useEffect(() => {
        
        if(withLoader === false) {
            setInfo("Purchase completed. Thanks for buying with us!");
            clearTimeout(timer1);
            setCompleted(true)
        }
        if(completed) {
            localStorage.removeItem("cart");
            window.setTimeout(() => {
                return navigate("/");
             }, 2000)
           
        }
        
    })

    if (type === "loader") {
        return (
            <div className="loader__root">
                <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
            </div>
        );
    } else if (type === "info") {
        return (
            <div className="loader__root--info">
                <div className="loader__cnt">
                    {withLoader && <div className="lds-ring lds-ring--info"><div></div><div></div><div></div><div></div></div>}
                    <div className="loader__cnt__info">
                        {info}
                    </div>
                </div>
            </div>
        );
    }

}

Loader.propTypes = {
    type: PropTypes.string.isRequired,
}

export default Loader;