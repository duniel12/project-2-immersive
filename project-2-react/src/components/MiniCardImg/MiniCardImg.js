import PropTypes from 'prop-types';
const MiniCardImg = (props) => {
    const { image, altImage, color, changeColor, actualColor } = props;


    return (
        <div className="mini-image__cnt">

            <button onClick={() => changeColor(color, false)} onMouseEnter={() => changeColor(color, true)}
        onMouseLeave={() => changeColor(actualColor, false)} className="mini-image__button">
                <img className="mini-image__button__img" src={image} alt={altImage} />
            </button>
        </div>
    );
}
MiniCardImg.propTypes = {
    image: PropTypes.string,
    altImage: PropTypes.string,
    color: PropTypes.string,
    changeColor: PropTypes.func,
    actualColor: PropTypes.string,
}

export default MiniCardImg;