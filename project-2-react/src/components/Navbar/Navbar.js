import { useState } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from "prop-types";
import {ReactComponent as CartSVG} from '../../assets/cart-svg.svg';
import Search from '../Search/Search';
import CartModal from '../CartModal/CartModal';

const Navbar = (props) => {
    const {onClickSearch, justLogo} = props;
    const [hovering, setHovering] = useState(true);

    const cartHover = (type) => {
        if(type === "enter") {
            setHovering(true);
        } else if(type === "leave") {
            setHovering(false);
        }
    }

    if(justLogo) {
        return (
            <header>
                <nav className="nav__root">
                    <Link to={"/"} className="nav__logo">GymRon</Link>
                </nav>
            </header>
        );
    } else {
        return (
            <header>
                <nav className="nav__root">
                    <Link  to={"/"} className="nav__logo">GymRon</Link>
                    <ul className="nav__links__root">
                        <li>
                            <Search onClickSearch={onClickSearch}/>
                        </li>
    
                        <li aria-label="link-for-products-nav">
                            <Link className="nav__links" to={"/products"}>
                                Products
                            </Link>
                        </li>
                        <li aria-label="link-for-shopping-cart-nav">
                            <Link className="nav__links" onMouseEnter={() => cartHover("enter")} onMouseLeave={() => cartHover("leave")}aria-label="cart-button" to={"/cart"}>
                                {<CartSVG />}
                            </Link>
                            {hovering && 
                            <div>
                                <CartModal hovering={hovering}/>
                            </div>}
                        </li>
    
                    </ul>
                </nav>
            </header>
        );
    }
}

Navbar.propTypes = {
    onClickSearch: PropTypes.func,
}

export default Navbar;