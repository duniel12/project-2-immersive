import { useContext, useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

import { ApiContext } from '../Context/ApiContextProvider';
import Navbar from '../Navbar/Navbar';
import Footer from '../Footer/Footer';
import SearchResult from '../SearchResult/SearchResult';
import CarouselLoader from '../Carousel/CarouselLoader';
import MiniCardImg from '../MiniCardImg/MiniCardImg';

const Product = () => {
    const { productId } = useParams();
    const { products } = useContext(ApiContext);
    const [search, setSearch] = useState(false);
    const navigate = useNavigate();

    const product = getProduct(productId, products);
    let firstKeyImg = Object.keys(product.images[0])[0];
    let secondKeyImg = Object.keys(product.images[1])[0];
    let thirdKeyImg = Object.keys(product.images[2])[0];

    let productImgs = product.images[0];
    //console.log(product.sizes);
    const [color, setColor] = useState(firstKeyImg);
    const [hoverColor, setHoverColor] = useState(null);
    const [hovering, setHovering] = useState(false);
    const [actualImages, setActualImages] = useState(product.images[0][firstKeyImg]);
    const [size, setSize] = useState(null);
    const [warning, setWarning] = useState(false);

    //Toggle search section
    const onClickSearch = () => {
        if (search === false) {
            setSearch(true);
        } else {
            setSearch(false);
        }
    }

    const changeColor = (color, hovering) => {
        if (hovering) {
            setHovering(true);
            setHoverColor(color);
        } else {
            setHovering(false);
            setColor(color);
        }

    }
    const changeSize = (size) => {
        setSize(size);
    }
    const addToCart = () => {
        
        if (size === null) {
            setWarning(true);
        } else {
            let localCart = JSON.parse(localStorage.getItem('cart'));
            if (localCart !== null) {
                let sameItem = false;
                for (let i = 0; i < localCart.length; i++) {
                    if (localCart[i].product.name === product.name && localCart[i].size === size && localCart[i].color === color) {
                        localCart[i].quantity += 1;
                        sameItem = true;
                    }
                }

                if (sameItem) {
                    localStorage.setItem('cart', JSON.stringify(localCart));
                } else {
                    let newCartItem = { product: product, size: size, color: color, quantity: 1, image: actualImages[0] };
                    localCart.push(newCartItem);
                    localStorage.setItem('cart', JSON.stringify(localCart));
                }

            } else {
                let newCartItem = [{ product: product, size: size, color: color, quantity: 1, image: actualImages[0] }];
                localStorage.setItem('cart', JSON.stringify(newCartItem));
            }
            return navigate("/cart");
        }
    }

    useEffect(() => {
        //Changes images for Carousel
        if (color === firstKeyImg) {
            setActualImages(product.images[0][firstKeyImg]);
        } else if (color === secondKeyImg) {
            setActualImages(product.images[1][secondKeyImg]);
        } else if (color === thirdKeyImg) {
            setActualImages(product.images[2][thirdKeyImg])
        }
    }, [color, product.images, firstKeyImg, secondKeyImg, thirdKeyImg]);


    return (
        <div>
            <Navbar onClickSearch={onClickSearch} />
            {search && <SearchResult onClickSearch={onClickSearch} />}
            <div className="product__and__footer">

            
            {!search &&
                <div className="product__root">
                    <div className="product__cnt product__cnt__img">
                        <CarouselLoader images={actualImages} />
                    </div>
                    <div className="product__cnt product__cnt__info">
                        <div className="product__info">
                            <h1 className="product__info__name product__info__text product__info__text--color">{product.name}</h1>
                            <p className="product__info__price product__info__text product__info__text--color">${product.price}</p>
                        </div>
                        <div className="product__colors">
                            {!hovering && <p className="product__normal-text product__colors__text">COLOR: <span className="product__normal-text product__colors__text product__colors__text--change">{color}</span></p>}
                            {hovering && <p className="product__normal-text product__colors__text"> COLOR: <span className="product__normal-text product__colors__text  product__colors__text--change">{hoverColor}</span></p>}

                            <div className="product__colors__cards">
                                <MiniCardImg image={productImgs[firstKeyImg][0]} altImage={product.name + firstKeyImg} color=
                                    {firstKeyImg} changeColor={changeColor} actualColor={color} />
                                <MiniCardImg image={product.images[1][secondKeyImg][0]} altImage={product.name + secondKeyImg} color=
                                    {secondKeyImg} changeColor={changeColor} actualColor={color} />
                                <MiniCardImg image={product.images[2][thirdKeyImg][0]} altImage={product.name + thirdKeyImg} color=
                                    {thirdKeyImg} changeColor={changeColor} actualColor={color} />
                            </div>
                        </div>
                        <div className="product__line-break"></div>
                        <div className="product__sizes__root">
                            <p className="product__normal-text product__sizes__text ">Select size</p>
                            <div className="product__sizes__ctn">
                                {product.sizes.map((size, i) => {
                                    return (
                                        <button key={i} onClick={() => changeSize(size)} className="product__sizes__btn">
                                            {size}
                                        </button>
                                    )
                                })}
                            </div>
                        </div>
                        <div className="product__add__root">
                            <button onClick={addToCart} className="product__add__btn">
                                Add to cart
                            </button>

                            {warning && <p className="product__normal-text product__add__text--warning ">*Please select size to add to cart</p>}
                        </div>
                        <div className="product__general-info">
                            <p className="product__normal-text product__general-info__text">{product.description}</p>
                            <p className="product__normal-text product__general-info__text">Type: {product.type}</p>
                            <p className="product__normal-text product__general-info__text">Collection: {product.collection}</p>
                        </div>
                    </div>


                </div>
            }
            
            </div>
        </div>
    );
}

export default Product;


const getProduct = (id, products) => {
    let valProduct = null;
    products.forEach((product) => {
        if (id === product.id) {
            valProduct = product;
        }
    })
    return valProduct;
}