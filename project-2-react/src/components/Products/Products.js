import React, { useContext, useState } from 'react';

import { ApiContext } from '../Context/ApiContextProvider';
import Navbar from '../Navbar/Navbar';
import CardsGenerator from '../CardsGenerator/CardsGenerator';
import SearchResult from '../SearchResult/SearchResult';
import Footer from '../Footer/Footer';

const Products = () => {
    const { products } = useContext(ApiContext);
    const [search, setSearch] = useState(false);

    //Toggle search section
    const onClickSearch = () => {
        if (search === false) {
            setSearch(true);
        } else {
            setSearch(false);
        }
    }

    return (
        <div>
            <Navbar onClickSearch={onClickSearch} />
            {search && <SearchResult onClickSearch={onClickSearch} />}
            {!search &&
                <div className="products__root">

                    <h1 className="products__title">Products</h1>


                    <CardsGenerator products={products} />
                    <Footer />
                </div>
            }
        </div>

    );
}

export default Products;