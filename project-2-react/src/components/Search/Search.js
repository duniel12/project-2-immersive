import PropTypes from "prop-types";

const Search = (props) => {
    const {onClickSearch} = props;

    return (
        <div>
            <button aria-label="search-nav-button" onClick={() => {onClickSearch()}} className="search__btn">
                <i className="search__icon fa fa-search"></i>
            </button>
        </div>

    );
}

Search.propTypes = {
    onClickSearch: PropTypes.func,
}


export default Search;