import { useState, useEffect, useContext } from "react";
import PropTypes from "prop-types";

import { ApiContext } from "../Context/ApiContextProvider";
import CloseModal from "../CloseModal/CloseModal";
import CardsGenerator from "../CardsGenerator/CardsGenerator";

const SearchResult = (props) => {
    const {products} = useContext(ApiContext);
    const [filterProducts, setFilterProducts] = useState([]);
    const [filter, setFilter] = useState("");
    const { onClickSearch } = props;
    let handleChangeFilter = (e) => {
        setFilter(e.target.value);
        
    }
    useEffect(() => {
        setFilterProducts(products.filter((product) => product.name.toLocaleLowerCase().includes(filter.toLowerCase()) || product.category.toLocaleLowerCase().includes(filter.toLowerCase()) || product.collection.toLocaleLowerCase().includes(filter.toLowerCase()) || product.product.toLocaleLowerCase().includes(filter.toLowerCase()) || product.type.toLocaleLowerCase().includes(filter.toLowerCase()) ));
    },[products,filter]);

    const removeFilter = () => {
        setFilter("");
    }

    return (
        <div className="result__root">
            <div className="result__filter__cnt">
                <input className="result__filter__inp" placeholder="Search products by typing keywords"  value={filter} onChange={e => handleChangeFilter(e)} />
                <div className="result__filter__remove">
                    {filter.length !== 0 && <button className="result__filter__remove--clear" onClick={removeFilter}>Clear search</button>
                    }
                    <CloseModal onClickSearch={onClickSearch} />
                </div>
            </div>
            {filter.length !== 0 &&
                <div className="result__cards__cnt">
                    <CardsGenerator products={filterProducts} cardThemeClass={"dark"}/>
                </div>
            }

        </div>
    );
}

SearchResult.propTypes = {
    onClickSearch: PropTypes.func,
}

export default SearchResult;